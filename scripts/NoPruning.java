/**
 * This class is exactly like SeqClass.java (from an older version
 * of it) except that pruning is turned off for REPTree. Does not
 * support bagging/boosting.
 *
 * Usage: <output_type> <ARFF file>
 *  - if <output_type> starts with a p, the output will be identical
 *    to the format of using `-p 0` with Weka
 *
 * (c) 2016 Lauren Yu
 */

import weka.classifiers.trees.REPTree;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Utils;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;

import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

public class NoPruning {
    private boolean REGRESSION;
    private boolean PRINT_INSTANCES;

    private Instances training;
    private Instances testing;
    private REPTree tree = new REPTree();

    // For regressions
    private double[] actual = null;
    private double[] preds = null;

    // For classification
    private int truePos = 0;
    private int trueNeg = 0;
    private int falsePos = 0;
    private int falseNeg = 0;

    public NoPruning(String output, String arff) {
	PRINT_INSTANCES = output.startsWith("p");
	run(arff);
    }

    private void run(String arff) {
	// Train classifier
	try {
	    setup(arff);
	} catch (Exception e) {
	    System.out.println("\nError setting up classifier:\n" + e);
	    return;
	}

	// Test instances
	try {
	    testInstances();
	} catch (Exception e) {
	    System.out.println("\nError testing instances:\n" + e);
	}
    }

    private void setup(String arff) throws Exception {
	setupData(arff);
	tree.setNoPruning(true);
	tree.buildClassifier(training);

	if (PRINT_INSTANCES) {
	    printHeader();
	} else if (REGRESSION) {
	    actual = new double[testing.numInstances()];
	    preds = new double[testing.numInstances()];
	}
    }

    private void setupData(String arff) throws IOException {
	Instances instances = readARFF(arff);
	if (instances.checkForStringAttributes()) {
	    instances.deleteStringAttributes();
	}

	// Make training set out of first 25%
	int trainNum = instances.numInstances() / 4;

	training = new Instances(instances, trainNum);
	for (int i = 0; i < trainNum; i++) {
	    training.add(instances.firstInstance());
	    instances.delete(0);
	}

	// Testing set is the remaining 75%
	testing = instances;

	// regression vs. classifier
	REGRESSION = (instances.numClasses() == 1);
    }

    private Instances readARFF(String arff) throws IOException {
	FileReader fr = new FileReader(arff);
	Instances instances = new Instances(fr);
	instances.setClassIndex(instances.numAttributes() - 1);
	return instances;
    }

    private void testInstances() throws Exception {
	Vector<Double> prevLabels = new Vector<Double>();

	// Iterate through testing instances
	for (int i = 0; i < testing.numInstances(); i++) {
	    // Set previous predictions as attributes
	    Instance inst = testing.instance(i);
	    setPrevLabels(inst, prevLabels);

	    // Test instance
	    double[] res = tree.distributionForInstance(inst);
	    double label = getResult(res);

	    // Update prevLabels
	    prevLabels.add(label);
	    if (prevLabels.size() > 4) {
		prevLabels.remove(0);
	    }

	    // Print instance or update metrics
	    if (PRINT_INSTANCES) {
		printResult(inst, i, label, res);
	    } else if (REGRESSION) {
		updateArrays(inst, label, i);
	    } else {
		updateMatrix(inst, label);
	    }
	}

	// Print output (if not printing each instance)
	if (!PRINT_INSTANCES) {
	    printSummary();
	}
    }

    private void setPrevLabels(Instance inst, Vector<Double> prevLabels) {
	int s = prevLabels.size();

	for (int i = 0; i < s; i++) {
	    int j = inst.numAttributes() - s - 1 + i;
	    inst.setValue(j, prevLabels.get(i));
	}
    }

    private double getResult(double[] res) {
	if (REGRESSION) {
	    return res[0];
	} else {
	    return res[0] < res[1] ? 1 : 0;
	}
    }

    private void updateArrays(Instance inst, double label, int i) {
	actual[i] = inst.classValue();
	preds[i] = label;
    }

    private void updateMatrix(Instance inst, double label) {
	if (label == inst.classValue()) {
	    if (label == 0.0) {
		truePos++;
	    } else {
		trueNeg++;
	    }
	} else {
	    if (label == 0.0) {
		falsePos++;
	    } else {
		falseNeg++;
	    }
	}
    }

    private void printHeader() {
	if (REGRESSION) {
	    System.out.println("\n\n=== Predictions on test data ===\n\n inst#     actual  predicted      error");
	} else {
	    System.out.println("\n\n=== Predictions on test data ===\n\n inst#     actual  predicted error prediction");
	}
    }

    private void printResult(Instance inst, int i, double label, double[] res) {
	if (REGRESSION) {
	    String actual = Utils.doubleToString(inst.classValue(), 5, 2);
	    String pred = Utils.doubleToString(label, 5, 3);
	    String error = Utils.doubleToString(label - inst.classValue(), 6, 3);
	    System.out.format(" %5d     %s       %s     %s%n", i+1, actual, pred, error);
	} else {
	    String actual = ((int) inst.classValue() + 1) + ":" + inst.toString(inst.classIndex());
	    String pred = ((int) label + 1) + ":" + (int) label;
	    String error = actual.equals(pred) ? "       " : "   +   ";
	    String dist = Utils.doubleToString(res[(int) label], 3);
	    System.out.format(" %5d        %s        %s%s%s%n", i+1, actual, pred, error, dist);
	}
    }

    private void printSummary() {
	System.out.println(tree.toString());
	System.out.println("Number of training instances: " + training.numInstances());
	System.out.println("Number of testing instances: " + testing.numInstances());

	if (REGRESSION) {
	    PearsonsCorrelation corr = new PearsonsCorrelation();
	    double cc = corr.correlation(actual, preds);
	    System.out.format("Correlation coefficient: %.5f%n", cc);
	} else {
	    System.out.format("Accuracy: %.5f%n%n", (double) (truePos + trueNeg) / testing.numInstances());
	    System.out.println("=== Confusion Matrix ===\n");
	    System.out.println("    a    b   <-- classified as");
	    System.out.format(" %4d %4d |    a = 0%n", truePos, falseNeg);
	    System.out.format(" %4d %4d |    b = 1%n", falsePos, trueNeg);
	}
    }

    public static void main(String[] args) {
	if (args.length < 2) {
	    System.out.println("Not enough arguments. Usage: <output type> ARFF file");
	}

	NoPruning t = new NoPruning(args[0], args[1]);
    }
}

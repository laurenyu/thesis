'''
input: MusicXML, class {0:position, 1:bow-bridge, 2:off-the-string}, optional number of previous labels, optional division/unit
output: arff

(c) 2016 Lauren Yu
'''

import sys
import parser

# Writes header of the ARFF file
def write_header(arff, relation, classlabel, numlabels):
    arff.write("@RELATION " + relation + "\n\n")

    attr_file = "feature_lists/attributes-{}.txt".format(numlabels)
    if classlabel == 2:
        attr_file = "feature_lists/attributes-classification-{}.txt".format(numlabels)

    try:
        attr = open(attr_file)
    except:
        sys.exit("Error opening " + attr_file)

    arff.write(attr.read())
    arff.write("\n\n@DATA\n")
    attr.close()

# Writes instances for the ARFF file
def write_instances(arff, xml, classlabel, numlabels, unit):
    parse = parser.Parser(xml, classlabel, numlabels, unit)
    parse.skip_first_notes()
    while True:
        try:
            instance = parse.next()
            arff.write(instance.to_arff() + "\n")
        except StopIteration:
            return

'''
Main method starts here
'''
unit = 0
numlabels = 8

# Check enough arguments
if len(sys.argv) == 5:
    unit = int(sys.argv[4])
    numlabels = int(sys.argv[3])
elif len(sys.argv) == 4:
    numlabels = int(sys.argv[3])
elif not len(sys.argv) == 3:
    sys.exit("Error: improper arguments. Usage: MusicXML file, class number, (optional) number of previous labels (default 8), (optional) unit")

# Set classlabel
classlabel = int(sys.argv[2])

# Open new file
relation = sys.argv[1][4:-4] + sys.argv[2]
arff = open("data/" + relation + ".arff", "a")

# Write everything
write_header(arff, relation, classlabel, numlabels)
write_instances(arff, sys.argv[1], classlabel, numlabels, unit)

# Close newly opened file
arff.close()

'''
Functions for dealing with pitches and note names
(c) 2016 Lauren Yu
'''

import xml.etree.ElementTree

def steps_from_A(pitch):
    steps = {"C": -9, "D": -7, "E": -5, "F": -4, "G": -2, "A": 0, "B": 2}

    # Parse pitch node
    note = pitch.find('step').text
    octave = int(pitch.find('octave').text)
    try:
        alter = int(pitch.find('alter').text)
    except:
        alter = 0

    return (octave-4) * 12 + steps[note] + alter

# Function to convert pitches encoded in MusicXML to frequency. Based on the
# formula from http://www.phy.mtu.edu/~suits/NoteFreqCalcs.html
def get_freq(pitch):
    # Constants
    a = 2.0 ** (1.0/12.0) # Set a to be the twelfth root of 2
    f0 = 440.0 # A4

    n = float(steps_from_A(pitch))

    return (f0 * (a ** n))

# Circle of fifths
circ = ["C", "G", "D", "A", "E", "B", "F#", "C#", "G#", "D#", "A#", "F"]

def get_tonic(key):
    global circ

    # Minor starts counting from A
    shift = 0
    mode = key.find('mode').text
    if mode == "minor":
        shift = 3

    n = int(key.find('fifths').text)
    return circ[(n + shift) % 12]

def get_charge(pitch, tonic):
    global circ
    note = pitch.find('step').text
    return (circ.index(note) - circ.index(tonic)) % 12

def get_interval(current, prev):
    if prev is None:
        return "0"
    else:
        return str(steps_from_A(current) - steps_from_A(prev))

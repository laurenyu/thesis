/**
 * This class uses Weka's REPTree. It copies the predicted
 * value into the "previous labels" attributes for each instance
 * before testing.
 *
 * Usage
 * -t training set. If no testing set is provided, then this set is split 25%/75%.
 * -T testing set. If no testing set is provided, then this set is split 25%/75%.
 * -b turns on bagging
 * -p prints an output that mimic's Weka's `-p 0` output (not including whitespace)
 *
 * (c) 2016 Lauren Yu
 */

import weka.classifiers.Classifier;
import weka.classifiers.meta.AdaBoostM1;
import weka.classifiers.meta.Bagging;
import weka.classifiers.trees.REPTree;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Utils;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;

import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

public class SeqClass {
    private boolean BAGGING = false;
    private boolean PRINT_INSTANCES = false;
    private boolean REGRESSION;

    private Instances training;
    private Instances testing;
    private Classifier tree;

    // For regressions
    private double[] actual = null;
    private double[] preds = null;

    // For classification
    private int truePos = 0;
    private int trueNeg = 0;
    private int falsePos = 0;
    private int falseNeg = 0;

    public SeqClass(String[] args) {
	parseArgs(args);
	run();
    }

    private void parseArgs(String[] args) {
	for (int i = 0; i < args.length; i++) {
	    String token = args[i];
	    if (token.equals("-t")) {
		try {
		    training = readARFF(args[++i]);
		} catch (Exception e) {
		    System.out.println("Error reading training file");
		    System.exit(0);
		}
	    } else if (token.equals("-T")) {
		try {
		    testing = readARFF(args[++i]);
		} catch (Exception e) {
		    System.out.println("Error reading testing file");
		    System.exit(0);
		}
	    } else if (token.startsWith("-")) {
		for (int j = 0; j < token.length(); j++) {
		    char flag = token.charAt(j);
		    switch (flag) {
		    case 'b':
			BAGGING = true;
			break;
		    case 'p':
			PRINT_INSTANCES = true;
		    }
		}
	    }
	}

	if (training == null) {
	    System.out.println("Error: no training file");
	    System.exit(0);
	}
    }

    private void run() {
	// Train classifier
	try {
	    setup();
	} catch (Exception e) {
	    System.out.println("\nError setting up classifier:\n" + e);
	    return;
	}

	// Test instances
	try {
	    testInstances();
	} catch (Exception e) {
	    System.out.println("\nError testing instances:\n" + e);
	}
    }

    private void setup() throws Exception {
	setupData();
	setupClassifier();
	tree.buildClassifier(training);

	if (PRINT_INSTANCES) {
	    printHeader();
	} else if (REGRESSION) {
	    actual = new double[testing.numInstances()];
	    preds = new double[testing.numInstances()];
	}
    }

    private void setupData() throws IOException {
	// regression vs. classifier
	REGRESSION = (training.numClasses() == 1);

	if (training.checkForStringAttributes()) {
	    training.deleteStringAttributes();
	}

	if (testing != null) {
	    if (testing.checkForStringAttributes()) {
		testing.deleteStringAttributes();
	    }
	    return;
	}

	Instances instances = training;

	// Make training set out of first 25%
	int trainNum = instances.numInstances() / 4;

	training = new Instances(instances, trainNum);
	for (int i = 0; i < trainNum; i++) {
	    training.add(instances.firstInstance());
	    instances.delete(0);
	}

	// Testing set is the remaining 75%
	testing = instances;
    }

    private void setupClassifier() throws Exception {
	if (BAGGING) {
	    setupBaggedClassifier();
	} else {
	    tree = new REPTree();
	}
    }

    private void setupBaggedClassifier() throws Exception {
	if (REGRESSION) {
	    tree = new Bagging(); // default base is REPTree
	} else {
	    tree = new AdaBoostM1();
	    String[] options = {"-W", "weka.classifiers.trees.REPTree"};
	    tree.setOptions(options);
	}
    }

    private Instances readARFF(String arff) throws IOException {
	FileReader fr = new FileReader(arff);
	Instances instances = new Instances(fr);
	instances.setClassIndex(instances.numAttributes() - 1);
	return instances;
    }

    private void testInstances() throws Exception {
	Vector<Double> prevLabels = new Vector<Double>();

	// Iterate through testing instances
	for (int i = 0; i < testing.numInstances(); i++) {
	    // Set previous predictions as attributes
	    Instance inst = testing.instance(i);
	    setPrevLabels(inst, prevLabels);

	    // Test instance
	    double[] res = tree.distributionForInstance(inst);
	    double label = getResult(res);

	    // Update prevLabels
	    prevLabels.add(label);
	    if (prevLabels.size() > 4) {
		prevLabels.remove(0);
	    }

	    // Print instance or update metrics
	    if (PRINT_INSTANCES) {
		printResult(inst, i, label, res);
	    } else if (REGRESSION) {
		updateArrays(inst, label, i);
	    } else {
		updateMatrix(inst, label);
	    }
	}

	// Print output (if not printing each instance)
	if (!PRINT_INSTANCES) {
	    printSummary();
	}
    }

    private void setPrevLabels(Instance inst, Vector<Double> prevLabels) {
	int s = prevLabels.size();

	for (int i = 0; i < s; i++) {
	    int j = inst.numAttributes() - s - 1 + i;
	    inst.setValue(j, prevLabels.get(i));
	}
    }

    private double getResult(double[] res) {
	if (REGRESSION) {
	    return res[0];
	} else {
	    return res[0] < res[1] ? 1 : 0;
	}
    }

    private void updateArrays(Instance inst, double label, int i) {
	actual[i] = inst.classValue();
	preds[i] = label;
    }

    private void updateMatrix(Instance inst, double label) {
	if (label == inst.classValue()) {
	    if (label == 0.0) {
		truePos++;
	    } else {
		trueNeg++;
	    }
	} else {
	    if (label == 0.0) {
		falsePos++;
	    } else {
		falseNeg++;
	    }
	}
    }

    private void printHeader() {
	if (REGRESSION) {
	    System.out.println("\n\n=== Predictions on test data ===\n\n inst#     actual  predicted      error");
	} else {
	    System.out.println("\n\n=== Predictions on test data ===\n\n inst#     actual  predicted error prediction");
	}
    }

    private void printResult(Instance inst, int i, double label, double[] res) {
	if (REGRESSION) {
	    String actual = Utils.doubleToString(inst.classValue(), 5, 2);
	    String pred = Utils.doubleToString(label, 5, 3);
	    String error = Utils.doubleToString(label - inst.classValue(), 6, 3);
	    System.out.format(" %5d     %s       %s     %s%n", i+1, actual, pred, error);
	} else {
	    String actual = ((int) inst.classValue() + 1) + ":" + inst.toString(inst.classIndex());
	    String pred = ((int) label + 1) + ":" + (int) label;
	    String error = actual.equals(pred) ? "       " : "   +   ";
	    String dist = Utils.doubleToString(res[(int) label], 3);
	    System.out.format(" %5d        %s        %s%s%s%n", i+1, actual, pred, error, dist);
	}
    }

    private void printSummary() {
	System.out.println(tree.toString());
	System.out.println("Number of training instances: " + training.numInstances());
	System.out.println("Number of testing instances: " + testing.numInstances());

	if (REGRESSION) {
	    PearsonsCorrelation corr = new PearsonsCorrelation();
	    double cc = corr.correlation(actual, preds);
	    System.out.format("Correlation coefficient: %.5f%n", cc);
	} else {
	    System.out.format("Accuracy: %.5f%n%n", (double) (truePos + trueNeg) / testing.numInstances());
	    System.out.println("=== Confusion Matrix ===\n");
	    System.out.println("    a    b   <-- classified as");
	    System.out.format(" %4d %4d |    a = 0%n", truePos, falseNeg);
	    System.out.format(" %4d %4d |    b = 1%n", falsePos, trueNeg);
	}
    }

    public static void main(String[] args) {
	if (args.length == 0) {
	    System.out.println("Not enough arguments.");
	    System.exit(0);
	}

	SeqClass t = new SeqClass(args);
    }
}

'''
Functions to calculate the strength of the beat.
Inputs:
- top      top number in time signature
- bottom   bottom number in time signature
- division number given by the divisions tag in MusicXML
- beat     the number of divisions that have already passed in the measure

(c) 2016 Lauren Yu
'''

def arity(top):
    if top % 2 == 0:
        return 2
    elif top % 3 == 0:
        return 3
    else:
        return top

# Modeled after the example for 4/4 in Marchini et al. Should also
# work for 3/4
def quarters(top, bottom, division, beat):
    if beat == division: # downbeat
        return 4;

    fpart = beat % division
    if fpart == 0:
        # on the beat
        a = arity(top)
        if (beat // division) % a == 1:
            return 3 # odd-numbered beat of bar
        else:
            return 2 # even-numbered beat of bar
    else:
        # check if half the beat
        if fpart == division // 2:
            return 1 # eighth note
        else:
            return 0 # smaller than an eighth note


# Should work for 6/8, 9/8, 12/8, etc.
def eighths(top, bottom, division, beat):
    division /= 2 # since eighth notes are the beats now
    if beat == division * 3:
        return 4

    fpart = beat % division
    if fpart == 0:
        # on an eighth note beat
        bigbeat = (beat // division) // 3
        leftover = (beat // division) % 3
        if leftover == 0:
            a = arity(top)
            if bigbeat % a == 1:
                return 3 # odd-numbered big beat of bar
            else:
                return 2 # even-numbered big beat of bar
        else:
            return 1 # some other eighth note
    else:
        return 0 # smaller than an eighth note


def strengthof(top, bottom, division, beat):
    if bottom == 4:
        return quarters(top, bottom, division, beat)
    elif bottom == 8:
        return eighths(top, bottom, division, beat)
    else:
        return 0

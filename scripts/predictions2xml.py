'''
input: output of -p 0 from Weka, xml with labels, # of training instances, which label (0..2)
output: MusicXML with predicted labels
(c) 2016 Lauren Yu
'''

import sys
import xml.etree.ElementTree as etree

def set_unit(measure):
    division = int(measure.find('attributes').find('divisions').text)
    if len(sys.argv) == 7:
        return int(sys.argv[6])
    else:
        return 1 if division <= 4 else division // 4

def set_duration(note):
    if note.find('grace') is None:
        return int(note.find('duration').text)
    else:
        global unit
        return unit

def new_label():
    global j, k
    if j < k:
        j += 1
        return ""

    global preds, i, classlabel

    line = preds[i]
    i += 1

    label = line.strip().split()[2]
    if classlabel == 2:
        label = label[-1:]
    else:
        label = str(round(float(label),2))

    return label


# Actual code starts here
if len(sys.argv) not in [5, 6, 7]:
    sys.exit("Error: improper arguments. Usage: output of `-p 0` from Weka, MusicXML, number of training instances, class number, (optional) number of previous labels, (optional) division")

# Read arguments
preds = open(sys.argv[1]).readlines()
xml = etree.parse(sys.argv[2])
classlabel = int(sys.argv[4])

# Counters for reading results
i = 5
j = 0

# Counter for starting in the right place
k = int(sys.argv[5]) if len(sys.argv) > 5 else 4
k += int(sys.argv[3])

# Setup
measures = xml.getroot().find('part').findall('measure')
unit = set_unit(measures[0])

for measure in measures:
    notes = measure.findall('note')
    for note in notes:
        if note.find('lyric') is None:
            continue

        # Generate new lyric
        place = 0
        duration = set_duration(note)
        lyric = ""
        while place < duration:
            lyric += "/{}".format(new_label())
            place += unit

        if lyric.endswith("/"):
            lyric = ""

        # Set new lyric
        lyrics = note.findall('lyric')
        lyrics[classlabel].find('text').text = lyric[1:]

if not i == len(preds):
    print("Error: did not reach end of predictions")

# Write new file
filename = sys.argv[2]
if not filename.startswith("xml/p-"):
    filename = "xml/p-" + filename[4:]
xml.write(filename)

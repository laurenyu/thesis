from pybrain.datasets import SupervisedDataSet
from pybrain.rl.learners import Q

import parser
import sys

def create_dataset(xml, composer, unit):
    data = SupervisedDataSet(31, 2)
    parse = parser.Parser(xml, composer, 3, unit)

    while True:
        try:
            instance = parse.next().to_pybrain()
            data.addSample(instance[0], instance[1])
        except StopIteration:
            return data

'''
MAIN METHOD STARTS HERE
'''
unit = 0
if len(sys.argv) == 4:
    unit = int(sys.argv[3])
elif not len(sys.argv) == 3:
    sys.exit("Error: improper arguments. Usage: MusicXML file, composer, (optional) unit")

data = create_dataset(sys.argv[1], sys.argv[2], unit)
learner = Q()

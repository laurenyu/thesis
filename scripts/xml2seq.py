'''
Creates .seq files. Currently hardcoded for 4 previous labels.

input: MusicXML, instances per measure, optional {0:position, 1:bow-bridge, 2:off-the-string, 3:all (default)}, optional division/unit
output: seq for JaHMM

(c) 2016 Lauren Yu
'''

import sys
import parser

# Writes instances for the ARFF file
def write_instances(arff, xml, classlabel, unit, measure):
    parse = parser.Parser(xml, classlabel, 4, unit)
    i = 0
    while True:
        try:
            instance = parse.next()
            arff.write(instance.to_seq())
            i += 1
            if i % measure == 0 and (i // measure) % 8 == 0:
                arff.write('\n')
        except StopIteration:
            return

'''
Main method starts here
'''
unit = 0
classlabel = 3

# Check enough arguments
if len(sys.argv) == 5:
    unit = int(sys.argv[4])
elif len(sys.argv) == 4:
    classlabel = int(sys.argv[3])
elif not len(sys.argv) == 3:
    sys.exit("Error: improper arguments. Usage: MusicXML file, instances per measure, (optional) class number, (optional) unit")

# Open new file
relation = sys.argv[1][4:-4] + str(classlabel)
seq = open("data/" + relation + ".seq", "a")

# Write everything
write_instances(seq, sys.argv[1], classlabel, unit, int(sys.argv[2]))

# Close newly opened file
seq.close()

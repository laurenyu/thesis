# Multiplies the duration of a note by some factor so that durations from two different
# pieces actually make sense. Usage: <ARFF file> <factor to multiply by>
# (c) 2016 Lauren Yu

import sys

if len(sys.argv) < 3:
    sys.exit("Error: not enough arguments. Usage: <ARFF file> <factor to multiply by>")

arff = open(sys.argv[1]).readlines()
factor = int(sys.argv[2])
header = True

for line in arff:
    line = line.strip()

    if header:
        print(line)
        if line == "@DATA":
            header = False
    else:
        attr = line.split(",")
        duration = factor * int(attr[9])
        print("{},{},{}".format(",".join(attr[0:9]), duration, ",".join(attr[10:])))

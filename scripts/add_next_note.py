"""
Adds next note's features to the current note's. Usage: <arff file> <class label>
(c) 2016 Lauren Yu
"""

import sys

# Writes header of the ARFF file
def write_header(arff, relation, classlabel):
    arff.write("@RELATION " + relation + "\n\n")

    attr_file = "feature_lists/next_note-regression.txt"
    if classlabel == 2:
        attr_file = "feature_lists/next_note-classification.txt"

    try:
        attr = open(attr_file)
    except:
        sys.exit("Error opening " + attr_file)

    arff.write(attr.read())
    arff.write("\n\n@DATA\n")
    attr.close()

'''
MAIN METHOD STARTS HERE
'''
if len(sys.argv) < 3:
    sys.exit("Not enough arguments. Usage: <ARFF file> <class label>")

filename = sys.argv[1]
orig = open(filename).readlines()
arff = open(filename[:-5] + "n.arff", "a")

classlabel = int(sys.argv[2])
write_header(arff, filename[5:-5], classlabel)

read = False
prev = None
for line in orig:
    line = line.strip()
    if line == "@DATA":
        read = True
        continue
    if read == False:
        continue

    attr = line.split(",")
    if prev is not None:
        arff.write("{},{},{},{}\n".format(",".join(prev[:-1]), ",".join(attr[1:3]), ",".join(attr[4:20]), prev[-1]))

    prev = attr

arff.close()

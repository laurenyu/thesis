"""
Takes an ARFF file for bow-bridge and discretizes the labels. Possible labels become:
{0, 0.5, 1, 1.5, 2}. Assumes 8 previous labels.
(c) 2016 Lauren Yu
"""

import sys

# Writes header of the ARFF file
def write_header(arff, relation):
    arff.write("@RELATION " + relation + "\n\n")

    attr_file = "feature_lists/attributes.txt"
    try:
        attr = open(attr_file)
    except:
        sys.exit("Error opening " + attr_file)

    arff.write(attr.read())
    arff.write("\n\n@DATA\n")
    attr.close()

def round_label(label):
    n = float(label)
    if n < 0.25:
        n = 0.0
    elif n < 0.75:
        n = 0.5
    elif n < 1.25:
        n = 1.0
    elif n < 1.75:
        n = 1.5
    else:
        n = 2
    return str(n)


'''
MAIN METHOD STARTS HERE
'''
if len(sys.argv) < 1:
    sys.exit("Not enough arguments. Usage: <ARFF file>")

filename = sys.argv[1]
orig = open(filename).readlines()
arff = open(filename[:-5] + "d.arff", "a")
write_header(arff, filename[5:-5])

read = False
for line in orig:
    line = line.strip()
    if line == "@DATA":
        read = True
        continue
    if read == False:
        continue

    attr = line.split(",")
    labels = attr[-9:]
    for i in range(9):
        labels[i] = round_label(labels[i])
    arff.write("{},{}\n".format(",".join(attr[:-9]), ",".join(labels)))

arff.close()

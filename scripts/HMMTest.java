// Runs JaHMM. Uses K-means to produce an initial HMM and Baum-Welch to improve it.
// Usage: <.seq file> <number of states>
// (c) 2016 Lauren Yu

import be.ac.ulg.montefiore.run.jahmm.Hmm;
import be.ac.ulg.montefiore.run.jahmm.ObservationVector;
import be.ac.ulg.montefiore.run.jahmm.OpdfMultiGaussianFactory;
import be.ac.ulg.montefiore.run.jahmm.learn.BaumWelchLearner;
import be.ac.ulg.montefiore.run.jahmm.learn.KMeansLearner;

import be.ac.ulg.montefiore.run.jahmm.io.FileFormatException;
import be.ac.ulg.montefiore.run.jahmm.io.ObservationVectorReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;

import java.util.ArrayList;
import java.util.List;

public class HMMTest {
    // Number of values in vector
    private int dimension = 34;

    // Sequences/observations
    private List<List<ObservationVector>> seqs = new ArrayList<List<ObservationVector>>();

    public HMMTest(String file, int states) {
	try {
	    readFile(file);
	} catch (IOException | FileFormatException e) {
	    System.out.println("Error reading file:\n" + e);
	    return;
	}

	Hmm<ObservationVector> hmm = train(states);
	System.out.println(hmm);
    }

    // Read input file and create list of ObservationVectors
    private void readFile(String file) throws IOException, FileFormatException {
	ObservationVectorReader obsReader = new ObservationVectorReader();
	FileReader reader = new FileReader(file);
	StreamTokenizer tokenizer = new StreamTokenizer(reader);

	List<ObservationVector> obs = new ArrayList<ObservationVector>();
	while (reader.ready()) {
	    obs.add(obsReader.read(tokenizer));
	}

	dimension = obs.get(0).dimension();
	seqs.add(obs);
    }

    // Learn an HMM
    private Hmm<ObservationVector> train(int states) {
	KMeansLearner<ObservationVector> kml = new KMeansLearner<ObservationVector>(states, new OpdfMultiGaussianFactory(dimension), seqs);
	Hmm<ObservationVector> hmm = kml.iterate();
	BaumWelchLearner bwl = new BaumWelchLearner();
	hmm = bwl.iterate(hmm, seqs);
	return hmm;
    }

    public static void main(String[] args) {
	if (args.length < 2) {
	    System.out.println("Error: not enough arguments. Usage: <.seq file> <number of states>");
	    return;
	}
	int states = Integer.parseInt(args[1]);

	HMMTest t = new HMMTest(args[0], states);
    }
}

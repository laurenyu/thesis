'''
Iterator that parses MusicXML and returns instances one at a time via next()
(c) 2016 Lauren Yu
'''

import instance
import pitchfunc
import xml.etree.ElementTree as etree

class Parser():
    def __init__(self, filename, classlabel, numlabels, unit):
        self.classlabel = classlabel # which line of the lyrics to use
        self.numlabels = numlabels # number of previous labels

        # Parse xml for measures and notes
        xml = etree.parse(filename)
        self.measures = xml.getroot().find('part').findall('measure')
        self.notes = self.measures[0].getchildren()

        # General things about the piece that need to be stored for future computation
        meta = self.measures[0].find('attributes')
        self.tonic = pitchfunc.get_tonic(meta.find('key'))
        self.division = int(meta.find('divisions').text)
        self.beats = int(meta.find('time').find('beats').text)
        self.beattype = int(meta.find('time').find('beat-type').text)

        if unit == 0:
            self.unit = 1 if self.division <= 4 else self.division // 4
        else:
            self.unit = unit

        # Other things to keep track of
        self.reset_beat()
        self.labels = None
        self.place = None
        self.duration = 0
        self.prevlabels = []
        self.intervals = []

        # For calculating previous interval
        self.pitch = None
        self.prev = None

        # Counters
        self.i = 0 # measure number
        self.j = -1 # note number
        self.k = 0 # instance number within note (for instance ID)

        # Instance that gets modified and returned
        self.cur = instance.Instance()

        # Set composer
        composer = xml.getroot().find('identification').find('creator').text.split()[-1]
        self.cur.set_composer(composer) # this will be the same for every instance

    # Calls next() so that there will be enough in the
    # lists with information about previous instances
    def skip_first_notes(self):
        for i in range(0, self.numlabels):
            self.next()

    # Returns the next instance. StopIteration is raised in the next_note()
    # method, which gets called by setup_new_note(), which gets called by this
    def next(self):
        if self.place is not None:
            self.cur.update_prev_dynamic()

        # Check if we're in the middle of a note
        # place will be None only if this is the very first time
        if (self.place is None) or (self.place >= self.duration):
            self.setup_new_note()

        # Set instance ID
        self.k += 1
        self.cur.set_id("{}.{}.{}".format(self.i, self.j, self.k))

        # Set attributes for instance
        self.cur.set_note_place(self.place, self.unit, self.duration)
        self.cur.set_intervals(self.intervals)
        self.cur.set_prev_labels(self.prevlabels)

        # Set label
        label = self.get_label()
        self.cur.set_label(label)

        # Update lists of previous note information
        self.update_lists(label)

        # Update place in note
        self.place += self.unit

        # Return instance
        return self.cur

    # Called when the beginning of a note is reached in next()
    def setup_new_note(self):
        self.cur.reset()
        self.beat += self.duration
        note = self.next_note()

        while note.tag == "direction":
            self.parse_direction_tag(note)
            note = self.next_note()

        while note.tag == "note" and note.find('lyric') is None:
            if note.find('rest') is not None:
                self.prevlabels.insert(0, "-1")

                if len(self.prevlabels) == (self.numlabels + 1):
                    self.prevlabels.pop()
                
            self.setup_new_note()
            return

        if note.tag == "note":
            self.place = 0

            self.set_duration(note)

            lyrics = note.findall('lyric')
            self.parse_lyrics(lyrics)

            self.pitch = note.find('pitch')
            self.cur.set_pitch(self.pitch, self.tonic)
            self.cur.set_duration(self.duration)
            self.cur.set_beat(self.beats, self.beattype, self.division, self.beat)
            self.parse_notations_tag(note.find('notations'))

        note = self.peek()
        while note is not None and (note.find('chord') is not None):
            self.set_as_chord(note)
            self.next_note()
            note = self.peek()

    # Returns next note but does not update i or j
    # Returns None when end is reached
    def peek(self):
        if self.j == len(self.notes) - 1:
            return None
        else:
            return self.notes[self.j + 1]

    # Returns next note and updates i and j appropriately
    # Returns None when end is reached
    def next_note(self):
        self.k = 0
        n = self.next_note_helper()
        while (not (n.tag == "direction")) and (not (n.tag == "note")):
            n = self.next_note_helper()

        return n

    def next_note_helper(self):
        if self.j == len(self.notes) - 1:
            self.j = 0
            if self.i == len(self.measures) - 1:
                raise StopIteration()
            else:
                self.i += 1
                self.reset_beat()
                measure = self.measures[self.i]
                self.notes = measure.getchildren()
        else:
            self.j += 1

        return self.notes[self.j]

    # Takes a direction tag and sets attributes if appropriate
    # dynamics, tempo, wedge (symbolic and text), text
    def parse_direction_tag(self, direction):
        sounds = direction.find('sound')
        if sounds is not None:
            if "dynamics" in sounds.attrib:
                self.cur.set_dynamic(sounds.attrib['dynamics'])
            if "tempo" in sounds.attrib:
                self.cur.set_tempo(sounds.attrib['tempo'])

        notations = direction.find('direction-type')
        if notations.find('wedge') is not None:
            wedge = notations.find('wedge').attrib['type']
            if wedge == "crescendo":
                self.cur.set_wedge("cresc")
            elif wedge == "diminuendo":
                self.cur.set_wedge("dim")

        if notations.find('dynamics') is not None:
            dyn = notations.find('dynamics').text
            if dyn in ["sf", "sfz", "sffz", "fz", "rf", "rfz"]:
                self.cur.set_accent()

        if notations.find('words') is not None:
            text = notations.find('words').text
            if "dim" in text or "decresc" in text:
                self.cur.set_wedge("dim")
            elif "cresc" in text:
                self.cur.set_wedge("cresc")
            elif len(text) > 0:
                self.cur.set_text(text)

    # Takes a notations tag and sets attributes if appropriate
    # slur, tenuto, accent, staccato, fermata, ornaments, bow-dir
    def parse_notations_tag(self, notations):
        if notations is None:
            return

        slur = notations.find('slur')
        if slur is not None:
            self.cur.set_slur(slur.attrib['type'])

        artc = notations.find('articulations')
        if artc is not None:
            if artc.find('tenuto') is not None:
                self.cur.set_tenuto()
            if artc.find('accent') is not None:
                self.cur.set_accent()
            if artc.find('staccato') is not None:
                self.cur.set_staccato()

        if notations.find('fermata') is not None:
            self.cur.set_fermata()
        if notations.find('ornaments') is not None:
            self.cur.set_ornament()

        technical = notations.find('technical')
        if technical is not None:
            if technical.find('up-bow') is not None:
                self.cur.set_bow_dir("up")
            elif technical.find('down-bow') is not None:
                self.cur.set_bow_dir("down")

    # Duration is set at the unit amount for grace notes. This is so
    # note-place will be set to "entire". For all other notes, the
    # duration tag is used to determine the note's duration.
    def set_duration(self, note):
        dur = 0
        if note.find('grace') is not None:
            dur = self.unit
        else:
            dur = int(note.find('duration').text)
        self.duration = dur

    # Parses lyric tags for labels and updates global list
    def parse_lyrics(self, lyrics):
        self.labels = []
        for i in range(3):
            self.labels.append(lyrics[i].find('text').text)

    # Changes the pitch and chord attributes of a note
    def set_as_chord(self, note):
        self.cur.set_chord()
        self.cur.set_pitch(note.find('pitch'), self.tonic)

    # Parses lyrics for the label and updates global list appropriately
    def get_label(self):
        res = []
        for i in range(3):
            label = self.labels[i]
            if "/" in label:
                n = label.find("/")
                res.append(label[:n])
                self.labels[i] = label[n+1:]
            else:
                res.append(self.labels[i])

        if self.classlabel == 3:
            res.pop()
            return res
        else:
            return res[self.classlabel]

    # Updates the lists of previous intervals and labels
    def update_lists(self, label):
        self.prevlabels.insert(0, label)
        self.intervals.insert(0, pitchfunc.get_interval(self.pitch, self.prev))

        if len(self.intervals) == 5:
            self.intervals.pop()

        if len(self.prevlabels) == (self.numlabels + 1):
            self.prevlabels.pop()

        self.prev = self.pitch

    # Resets beat according to if the main beat is an eighth note
    # or (assumed) quarter note
    def reset_beat(self):
        # Currently, this assumes there's a normal time signature (i.e. no 7/8)
        if self.beattype == 8:
            self.beat = int(self.division * 3 / 2)
        else:
            self.beat = self.division

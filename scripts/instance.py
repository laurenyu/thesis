'''
Class for an instance
(c) 2016 Lauren Yu
'''

import beatstrength
import copy
import pitchfunc

class Instance():
    # Only for the first one
    def __init__(self):
        self.attr = [None] * 20
        self.attr[9] = "none"
        self.attr[19] = "0"
        self.reset()

    # Called when starting a new note (but not necessarily a new instance)
    def reset(self):
        # slur
        if self.attr[9] == "start":
            self.attr[9] = "middle"
        elif self.attr[9] == "stop":
            self.attr[9] = "none"

        # wedge, text, bow-dir
        for i in [5, 6, 16]:
            self.attr[i] = "none"

        # tenuto, accent, staccato, fermata, ornament, chord
        for i in range (10, 16):
            self.attr[i] = "0"

    # Called before starting every new instance
    def update_prev_dynamic(self):
        self.attr[19] = self.attr[4]

    '''
    Methods for converting instances to file formats
    '''
    def to_arff(self):
        return "{},{},{},{},{}".format(self.id, ",".join(self.attr),
                                       ",".join(self.intervals),
                                       ",".join(self.prevlabels), self.label)

    def to_seq(self):
        num_attr = self.num_only()
        self.ensure_length()
        return "[{} {} {}];".format(" ".join(num_attr),
                                    " ".join(self.intervals),
                                    " ".join(self.label))

    def to_pybrain(self):
        self.ensure_length()
        input_values = self.num_only()
        input_values.extend(self.intervals)
        return (input_values, self.label)

    # Converts attribute list to one with only doubles/ints
    # limited number of attributes so JaHMM doesn't complain
    # about the matrix not being positive defined
    def num_only(self):
        res = []

        # pitch, charge
        for i in [0, 1]:
            res.append(self.attr[i])

        # dynamic
        res.append(self.attr[4])

        # beat strength
        res.append(self.attr[18])

        return res

    def ensure_length(self):
        while not len(self.intervals) == 4:
            self.intervals.append("-1")

    '''
    Methods for setting values of instances
    '''
    def set_id(self, instance_id):
        self.id = instance_id

    def set_pitch(self, pitch, tonic):
        if pitch is not None:
            self.attr[0] = str(pitchfunc.get_freq(pitch))
            self.attr[1] = str(pitchfunc.get_charge(pitch, tonic))

    def set_composer(self, composer):
        self.attr[2] = composer

    def set_note_place(self, place, unit, duration):
        if unit == duration:
            note_place = "entire"
        elif place == 0:
            note_place = "begin"
        elif place + unit >= duration:
            note_place = "end"
        else:
            note_place = "middle"

        self.attr[3] = note_place

    def set_dynamic(self, dynamic):
        self.attr[4] = dynamic

    def set_wedge(self, wedge):
        self.attr[5] = wedge

    def set_text(self, text):
        self.attr[6] = "\"{}\"".format(text)

    def set_tempo(self, tempo):
        self.attr[7] = tempo

    def set_duration(self, duration):
        self.attr[8] = str(duration)

    def set_slur(self, slur):
        self.attr[9] = slur

    def set_tenuto(self):
        self.attr[10] = "1"

    def set_accent(self):
        self.attr[11] = "1"

    def set_staccato(self):
        self.attr[12] = "1"

    def set_fermata(self):
        self.attr[13] = "1"

    def set_ornament(self):
        self.attr[14] = "1"

    def set_chord(self):
        self.attr[15] = "1"

    def set_bow_dir(self, direction):
        self.attr[16] = direction

    def set_beat(self, top, bottom, division, beat):
        self.attr[17] = str(beat)
        self.attr[18] = str(beatstrength.strengthof(top, bottom, division, beat))

    def set_intervals(self, intervals):
        self.intervals = copy.deepcopy(intervals)

    def set_prev_labels(self, prevlabels):
        self.prevlabels = copy.deepcopy(prevlabels)

    def set_label(self, label):
        self.label = label

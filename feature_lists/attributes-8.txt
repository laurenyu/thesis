@ATTRIBUTE instance_id	STRING
@ATTRIBUTE pitch        REAL
@ATTRIBUTE charge       INTEGER
@ATTRIBUTE composer     {Bach, Sibelius, Bartok, Wieniawski}
@ATTRIBUTE note-place	{begin, middle, end, entire}
@ATTRIBUTE dynamics     REAL
@ATTRIBUTE wedge        {cresc, dim, none}
@ATTRIBUTE text         STRING
@ATTRIBUTE tempo        INTEGER
@ATTRIBUTE duration     INTEGER
@ATTRIBUTE slur         {start, middle, stop, none}
@ATTRIBUTE tenuto       {0,1}
@ATTRIBUTE accent       {0,1}
@ATTRIBUTE staccato     {0,1}
@ATTRIBUTE fermata      {0,1}
@ATTRIBUTE ornament     {0,1}
@ATTRIBUTE chord	{0,1}
@ATTRIBUTE bow-dir      {up, down, none}
@ATTRIBUTE beat         INTEGER
@ATTRIBUTE bt-strength  INTEGER
@ATTRIBUTE 1dynamic	REAL
@ATTRIBUTE 0-1int	INTEGER
@ATTRIBUTE 1-2int	INTEGER
@ATTRIBUTE 2-3int	INTEGER
@ATTRIBUTE 3-4int	INTEGER
@ATTRIBUTE 1label	REAL
@ATTRIBUTE 2label	REAL
@ATTRIBUTE 3label	REAL
@ATTRIBUTE 4label 	REAL
@ATTRIBUTE 5label 	REAL
@ATTRIBUTE 6label 	REAL
@ATTRIBUTE 7label 	REAL
@ATTRIBUTE 8label 	REAL
@ATTRIBUTE label	REAL

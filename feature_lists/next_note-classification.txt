@ATTRIBUTE instance_id	STRING
@ATTRIBUTE pitch        REAL
@ATTRIBUTE charge       INTEGER
@ATTRIBUTE composer     {Bach, Sibelius, Bartok, Wieniawski}
@ATTRIBUTE note-place	{begin, middle, end, entire}
@ATTRIBUTE dynamics     REAL
@ATTRIBUTE wedge        {cresc, dim, none}
@ATTRIBUTE text         STRING
@ATTRIBUTE tempo        INTEGER
@ATTRIBUTE duration     INTEGER
@ATTRIBUTE slur         {start, middle, stop, none}
@ATTRIBUTE tenuto       {0,1}
@ATTRIBUTE accent       {0,1}
@ATTRIBUTE staccato     {0,1}
@ATTRIBUTE fermata      {0,1}
@ATTRIBUTE ornament     {0,1}
@ATTRIBUTE chord	{0,1}
@ATTRIBUTE bow-dir      {up, down, none}
@ATTRIBUTE beat         INTEGER
@ATTRIBUTE bt-strength  INTEGER
@ATTRIBUTE 1dynamic	REAL
@ATTRIBUTE 0-1int	INTEGER
@ATTRIBUTE 1-2int	INTEGER
@ATTRIBUTE 2-3int	INTEGER
@ATTRIBUTE 3-4int	INTEGER
@ATTRIBUTE 1label	{0,1,-1}
@ATTRIBUTE 2label	{0,1,-1}
@ATTRIBUTE 3label	{0,1,-1}
@ATTRIBUTE 4label 	{0,1,-1}
@ATTRIBUTE 5label 	{0,1,-1}
@ATTRIBUTE 6label 	{0,1,-1}
@ATTRIBUTE 7label 	{0,1,-1}
@ATTRIBUTE 8label 	{0,1,-1}
@ATTRIBUTE pitch1       REAL
@ATTRIBUTE charge1      INTEGER
@ATTRIBUTE note-place1	{begin, middle, end, entire}
@ATTRIBUTE dynamics1    REAL
@ATTRIBUTE wedge1       {cresc, dim, none}
@ATTRIBUTE text1        STRING
@ATTRIBUTE tempo1       INTEGER
@ATTRIBUTE duration1    INTEGER
@ATTRIBUTE slur1        {start, middle, stop, none}
@ATTRIBUTE tenuto1      {0,1}
@ATTRIBUTE accent1      {0,1}
@ATTRIBUTE staccato1    {0,1}
@ATTRIBUTE fermata1     {0,1}
@ATTRIBUTE ornament1    {0,1}
@ATTRIBUTE chord1	{0,1}
@ATTRIBUTE bow-dir1     {up, down, none}
@ATTRIBUTE beat1        INTEGER
@ATTRIBUTE bt-strength1 INTEGER
@ATTRIBUTE label	{0,1}

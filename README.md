# README #

This repository is for my honors thesis at Williams College. The goal of this thesis is to use machine learning to train a computer to take violin/viola music as input, and give bow position, distance from the bridge, and whether or not the bow leaves the string as outputs.

## Directories ##

* **data** contains `.arff` and `.seq` files.
* **feature_lists** contains text files with attributes for `.arff` files. There are all pretty much the same, except they have different numbers of previous labels or include features of the next note. Also, some are for classification as opposed to regression (for `off-the-string`).
* **results** contains output from Weka. These should be in the format you get if you run Weka from command line with `-p 0`.
* **scripts** has all the code (see explanation below).
* **xml** contains the labeled MusicXML pieces (including predictions).

## Naming Scheme ##

In data, results, and xml, there are some abbreviations after the name of the file.

* `{0, 1, 2}` indicates bow position, distance from bridge, or off the string (in that order)
* `{4, 8, 16}p` indicates the number of previous labels.
* `b` indicates the use of bagging.
* `t` indicates training on one of the Bach pieces and testing on the other
* `n` indicates incorporating the next note.

## Scripts ##

Used for parsing MusicXML:

* `parser.py`
* `beatstrength.py`
* `pitchfunc.py`
* `instance.py`

Used for creating data files:

* `xml2arff.py`
* `xml2seq.py`

Used for post-processing:

* `add_next_note.py`
* `discretize1.py`
* `normalize_duration.py`

Used for mapping predictions back to MusicXML:

* `predictions2xml.py`

Used for actually running experiments:

* `SeqClass.java`
* `NoPruning.java`
* `HMMTest.java`

From back when I thought I'd do reinforcement learning:

* `rl.py`
* `rlenv.py`
* `rltask.py`